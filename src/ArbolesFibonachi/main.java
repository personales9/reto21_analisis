/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArbolesFibonachi;

import java.util.Scanner;
import util.ufps.colecciones_seed.ArbolBinario;
import util.ufps.colecciones_seed.NodoBin;

/**
 *Este problema se realizó usando arboles binarios
 * debido a que los arboles fibonacci son simplemente eso
 * abroles binarios los cuales su informacion está definida por
 * la secuencia fibonacci <3
 * @author Rafael Sarmiento
 */
public class main {
    public static void main(String[] args) throws Exception {
        ArbolBinario<Integer> fibonacci  = new ArbolBinario();
        NodoBin<Integer> n = new NodoBin();
        System.out.println("Ingrese la raiz del Arbol Fibonacci:");
        Scanner sc = new Scanner(System.in);        
        int numero = sc.nextInt();
        if(fibonacci.esVacio()){
            System.out.println("[VACIO]  ");
            System.out.println(" ______\n" +
" /|_||_\\`.__\n" +
"(   _    _ _\\\n" +
"=`-(_)--(_)-");
        }
        n.setInfo(numero);
        fibonacci.setRaiz(n);
        ArbolesFib.crearArbolFibonacci(fibonacci.getRaiz());
        System.out.println("Calculo coste T(n) = " + ArbolesFib.iteraciones );
        System.out.println("\n =====");
    }
}
