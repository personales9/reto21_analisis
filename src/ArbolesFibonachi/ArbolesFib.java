/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ArbolesFibonachi;
//import java.util.*;
import java.util.Scanner;
import util.ufps.colecciones_seed.ArbolBinario;
import util.ufps.colecciones_seed.NodoBin;
import util.varios.BTreePrinter;
/**
 *
 * @author Rafael Sarmiento
 */
public class ArbolesFib {
    //estas iteraciones son para calcular la complejidad computacional
    public static int iteraciones = 0;
    /**
     * esto nos permite tener la secuencia fibonacci para agregar al arbol
     * @param n un entero que va a ser el punto donde empieza la secuencia fibonacci
     * @return 
     */
    public int fibonacci(int n){
        if (n<2) {
            return n;
        }
        return fibonacci(n-2) + fibonacci(n-1);
    }
    
    
    /**
     * este nos permite ya tener la creacion del arbol completo y tambien nos da
     * la complejidad dependiendo de la cantidad de ciclos que esta funcion
     * haga
     * @param n
     * @return
     * @throws Exception 
     */
     public static int crearArbolIteracciones(NodoBin<Integer> n) throws Exception {

        iteraciones++;
        
        int aux = n.getInfo();
        
        if (aux < 0) {
            throw new Exception("Índice fuera de rango ( Ingresaste un numero negativo ) ");
        }
        
        if (aux < 2) {
            return aux;
        }
        
        NodoBin<Integer> izq = new NodoBin(aux -2);
        NodoBin<Integer> der = new NodoBin(aux -1);
        n.setIzq(izq);
        n.setDer(der);
        
        int izquierda = crearArbolIteracciones(izq);
        int derecha = crearArbolIteracciones(der);
        
        return izquierda + derecha;
    }
    /**
     * y este ya literalmente lo deja impreso con el btreeprinter del proyecto 
     * seed, esta funcion solo imprime el arbol creado anteriormente
     * @param raiz
     * @throws Exception 
     */ 
    public static void crearArbolFibonacci(NodoBin<Integer> raiz) throws Exception {
        System.out.println("Fibonacci: " + crearArbolIteracciones(raiz));
        BTreePrinter.printNode(raiz);
    }
    
}
