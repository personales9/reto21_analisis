/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Natalidad;

import java.io.IOException;

/**
 *
 * @author Rafael Sarmiento
 */
public class Main {
    
    public static void main(String[] args) throws IOException {

        while (true) {

            Natalidad.input = Natalidad.s.readLine();

            Natalidad.posicion = 0;
            Nodo node = new Nodo(Natalidad.input.charAt(Natalidad.posicion++));
            if (node.valor == '.') break;

            Natalidad.leer(node);
            Natalidad.uno = Natalidad.dos = true;
            Natalidad.detectarUnHijo(node);
            Natalidad.detectarDosHijos(node);
            if (Natalidad.uno) System.out.print("1");
            if (Natalidad.dos) System.out.print("2");
            if (!Natalidad.uno && !Natalidad.dos) System.out.print("N");

            System.out.println();
        }

    }
    
}
