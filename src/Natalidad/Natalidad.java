/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Natalidad;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
/**
 *
 * @author Rafael Sarmiento
 */
class Nodo {
    char valor;
    Nodo izquierdo, derecho;
    Nodo(char x) { valor = x; }
    public String toString() {
        return valor + "";
        
        }
    }
public class Natalidad {
        
    
    static int posicion;
    static String input;
    static boolean uno, dos;
    final static BufferedReader s = new BufferedReader(new InputStreamReader(System.in));
    

    static void detectarDosHijos(Nodo node) {
        if (node == null || !dos) return;
        if ((node.izquierdo != null && node.derecho == null) || (node.izquierdo == null && node.derecho != null))
            dos = false;
        detectarDosHijos(node.izquierdo);
        detectarDosHijos(node.derecho);
    }

    static void detectarUnHijo(Nodo node) {
        if (node == null || !uno) return;
        if (node.izquierdo != null && node.derecho != null) { uno = false; return; }
        detectarUnHijo(node.izquierdo);
        detectarUnHijo(node.derecho);
    }

    static void leer(Nodo node) {
        if (input.charAt(posicion) != '.') {
            node.izquierdo = new Nodo(input.charAt(posicion++));
            leer(node.izquierdo);
        }
        posicion++;
        if (input.charAt(posicion) != '.') {
            node.derecho = new Nodo(input.charAt(posicion++));
            leer(node.derecho);
        }
    }
    

    
}
