/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.Iterator;

/**
 * Clase para el manejo de un iterador
 *
 * @author madarme
 */
public class IteratorLCD<T> implements Iterator<T> {

    private NodoD<T> cabezaIterador;
    private NodoD<T> posicion_Iterador;

    public IteratorLCD(NodoD<T> cabezaIterador) {
        this.cabezaIterador = cabezaIterador;
        this.posicion_Iterador = this.cabezaIterador.getSig();
    }

    @Override
    public boolean hasNext() {
        return this.posicion_Iterador != this.cabezaIterador;
    }

    @Override
    public T next() {
        if (this.hasNext()) {
            this.posicion_Iterador = this.posicion_Iterador.getSig();
            return this.posicion_Iterador.getAnt().getInfo();
        }
        //puede crear una excepción de tipo runtime
        return null;
    }

}
