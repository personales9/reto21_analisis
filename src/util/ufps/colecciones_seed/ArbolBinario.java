/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

import java.util.Iterator;

/**
 *
 * @author 
 */
public class ArbolBinario<T> {

    private NodoBin<T> raiz;

    public ArbolBinario() {
    }

    public NodoBin<T> getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }

    public int getContarHojas() {

        return this.getContarHojas(this.raiz);
    }
    
    public int getContarHojaF() {

        return this.getContarHojasF(this.raiz);
    }
    
    private int getContarHojasF(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = this.esHojaF(r) ? 1 : 0;
        return c + this.getContarHojasF(r.getIzq()) + this.getContarHojasF(r.getDer());
    }

    private int getContarHojas(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r) ? 1 : 0;
        return c + this.getContarHojas(r.getIzq()) + this.getContarHojas(r.getDer());
    }
    
    private boolean esHojaF(NodoBin<T> x) {
        return (x != null && x.getIzq() == null && x.getDer() == null && x.getInfo().equals(1));
    }

    private boolean esHoja(NodoBin<T> x) {
        return (x != null && x.getIzq() == null && x.getDer() == null);
    }

    /**
     *
     * @return un entero con la cantidad de hojas que están a la derecha
     */
    public int getCanHojasDerechas() {
//        return getCanHojasDerechas(this.raiz);
        return getCanHojasDerechas(this.raiz, false);

    }

    private int getCanHojasDerechas(NodoBin<T> r) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r.getDer()) ? 1 : 0;
        return c + this.getCanHojasDerechas(r.getDer()) + this.getCanHojasDerechas(r.getIzq());
    }

    public int getCanHojasDerechas(NodoBin<T> r, boolean derecho) {
        if (r == null) {
            return 0;
        }
        int c = this.esHoja(r) && derecho ? 1 : 0;
        return c + this.getCanHojasDerechas(r.getIzq(), false) + this.getCanHojasDerechas(r.getDer(), true);

    }

    public String getLukasiewicz() {
        if (this.esVacio()) {
            throw new RuntimeException("Árbol vacío");
        }
        return this.getLukasiewicz(raiz);
    }

    private String getLukasiewicz(NodoBin<T> r) {

        if (r == null) {
            return "b";
        }
        return "a" + this.getLukasiewicz(r.getIzq()) + this.getLukasiewicz(r.getDer());

    }

    public boolean esVacio() {
        return this.raiz == null;
    }

    /**
     * Recorridos: Linealizar el àrbol
     */
    public Iterator<T> preOrden() {
        ListaCD<T> l = new ListaCD();
        preOrden(l, this.raiz);
        return l.iterator();
    }

    private void preOrden(ListaCD<T> l, NodoBin<T> r) {
        if (r == null) {
            return;
        }
        l.insertarFin(r.getInfo());
        preOrden(l, r.getIzq());
        preOrden(l, r.getDer());
    }

    public Iterator<T> inOrden() {
        ListaCD<T> l = new ListaCD();
        inOrden(l, this.raiz);
        return l.iterator();
    }

    private void inOrden(ListaCD<T> l, NodoBin<T> r) {
        if (r == null) {
            return;
        }

        inOrden(l, r.getIzq());
        l.insertarFin(r.getInfo());
        inOrden(l, r.getDer());
    }

    public Iterator<T> posOrden() {
        ListaCD<T> l = new ListaCD();
        posOrden(l, this.raiz);
        return l.iterator();
    }

    private void posOrden(ListaCD<T> l, NodoBin<T> r) {
        if (r == null) {
            return;
        }

        posOrden(l, r.getIzq());
        posOrden(l, r.getDer());
        l.insertarFin(r.getInfo());
    }

    public int getCardinalidad() {
        int x[] = {0};
        this.getCardinalidad(this.raiz, x);
        return x[0];
    }

    /**
     * versión 1
     *
     * @param r nodo binario que referencia cada raìz de un subàrbol
     * @param x un objeto wrapper , en este caso es un vector de entero
     */
    public void getCardinalidad(NodoBin<T> r, int[] x) {
        if (r == null) {
            return;
        }
        x[0]++;
        //System.out.println("En recursión:"+x);
        this.getCardinalidad(r.getIzq(), x);
        this.getCardinalidad(r.getDer(), x);
    }

    /**
     * Versión 2: public int cardin(NodoBin<T> r) { if(r==null) return 0; return
     * 1+cardin(r.getIzq())+cardin(r.getDer()); }
     */
    public String getElementos_Niveles() {
        String msg = "";
        if (!this.esVacio()) {
            Cola<NodoBin<T>> direcciones = new Cola();
            direcciones.enColar(this.raiz);
            while (!direcciones.esVacia()) {
                NodoBin<T> r = direcciones.deColar();
                msg += r.getInfo().toString() + "\t";
                if (r.getIzq() != null) {
                    direcciones.enColar(r.getIzq());
                }
                if (r.getDer() != null) {
                    direcciones.enColar(r.getDer());
                }
            }

        }

        return msg;
    }

    public int getAltura() {
        return getAltura(this.raiz);

    }

    public int getAltura(NodoBin<T> n) {
        if (n == null) {
            return 0;
        }

        int n1 = getAltura(n.getIzq());
        int n2 = getAltura(n.getDer());
        if (n1 > n2) {
            return n1 + 1;
        }
        return n2 + 1;
    }

    public boolean compararAreaArboles(ArbolBinario<T> arbol2) {

        if (this.esVacio() || arbol2.esVacio()) {
            System.out.println("Arbol vacio");
        }

        int A = areaArbol(this);
        int B = areaArbol(arbol2);
        return A == B;

    }

    public int areaArbol(ArbolBinario<T> x) {
        return x.getAltura() * x.getContarHojas();
    }

    public boolean buscarDato(T dato) {
        return buscarDato(this.raiz, dato);
    }

    public boolean buscarDato(NodoBin<T> x, T dato) {
        if (x == null) {
            return false;
        }

        if (x.getInfo().equals(dato)) {
            return true;
        } else {
            return buscarDato(x.getIzq(), dato) || buscarDato(x.getDer(), dato);
        }

    }
    
    public ArbolBinario<T> clonarArbol(){
        ArbolBinario <T> clon=null;
        if(this.esVacio()){
            System.out.println("Arbol vacio");
        }else{
            clon=this.clonarArbol(this.raiz);
        }
        return clon;
    }
 
    public ArbolBinario clonarArbol(NodoBin<T>temp){
        NodoBin <T> tempClon;
        if(temp==null){
            tempClon=null;
        }else{
            ArbolBinario <T> izqClon, derClon;
            izqClon=clonarArbol(temp.getIzq());
            derClon=clonarArbol(temp.getDer());
            tempClon=new NodoBin <T>(temp.getInfo());
            tempClon.setIzq(izqClon.raiz);
            tempClon.setDer(derClon.raiz);
        }
        ArbolBinario <T> clon=new ArbolBinario<>();
        clon.raiz=tempClon;
        return clon;
    }
}
